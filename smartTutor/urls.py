"""smartTutor URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from myProfile import views
from django.conf import settings
from django.conf.urls.static import static
from quiz import views as quiz

urlpatterns = [
    url(r'^accounts/index', views.index,name='account_index' ),
    url(r'^learn/', include('simpleblog.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/profile',views.profile,name='account_profile' ),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^$',quiz.index,name='quiz_categories'),
    url(r'^category/(?P<category_id>\d+)', quiz.view_category,name='quiz_category'),
    url(r'^take/(?P<quiz_id>\d+)/$', quiz.quiz_take,name='quiz_take')
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
