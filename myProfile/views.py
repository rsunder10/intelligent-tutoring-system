from django.shortcuts import render
from .models import Experience

# Create your views here.
def index(request):
    return render(request, "account/index.html")
def profile(request):
    experience = Experience.objects.all().filter(user = request.user)
    total = 0
    print(experience)
    enjoy = 0
    upset = 0
    for i in experience:
        print(i.sentiment)
        if(i.sentiment == True):
            enjoy+=1
        else:
            upset += 1
    total =0.0
    total =total+upset+enjoy
    experience = (enjoy/total)*100
    danger = (upset/total)*100
    return render(request, "account/profile.html",{'enjoy':experience,'upset':danger})
