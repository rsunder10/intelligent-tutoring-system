import os  # manipulate paths
import pandas as pd  # SQL-like operations and convenience functions
import joblib  # save and load models
from sklearn.cross_validation import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.linear_model import LogisticRegressionCV
from sklearn.pipeline import Pipeline

training_csv_file = '/Users/rsunder/Desktop/UniversityProject2/smartTutor/simpleblog/training.1600000.processed.noemoticon.csv'


names = ('polarity', 'id', 'date', 'query', 'author', 'text')
df = pd.read_csv(training_csv_file, encoding='latin1', names=names)
df['polarity'].replace({0: -1, 4: 1}, inplace=True)
text = df['text']
target = df['polarity'].values

text_train, text_validation, target_train, target_validation = (
    train_test_split(text, target, test_size=0.2, random_state=42)
)

vectorizer = CountVectorizer(ngram_range=(1, 2), max_features=100000)
feature_selector = SelectKBest(chi2, k=5000)
classifier = LogisticRegressionCV(n_jobs=4)
print os.getcwd()
print os.path.exists('simpleblog/model.pkl')
if os.path.exists('simpleblog/model.pkl'):
    sentiment_pipeline = joblib.load('simpleblog/model.pkl')
else:
    sentiment_pipeline = Pipeline((
        ('v', vectorizer),
        ('f', feature_selector),
        ('c', classifier)
    ))
    sentiment_pipeline.fit(text_train, target_train)
    joblib.dump(sentiment_pipeline, 'simpleblog/model.pkl');


print(sentiment_pipeline.predict(["I am not able to understand the course"]))
